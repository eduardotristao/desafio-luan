## Devops

Challege made for study

## Requirements

- [SSH-key](https://gitlab.com/profile/keys)

## Installation

``` sh
git clone git@gitlab.com:eduardotristao/desafio-luan.git ~/devops
```

## Start VM

``` sh
vagrant up
```

## In case of any problem while installing Ansible, please try:

``` sh
vagrant up --provision
```

## Validate Docker Installation

``` sh
vagrant ssh
docker run hello-world
```

## Help 

If you have any further information, feel free to contact [me](eduardo.tristao@on3web.com.br) 

